<?php
namespace MatchMovePay\VCard;

use MatchMovePay\Helper\Exception;
use MatchMovePay\Helper\Curl;

class Connection 
{
    const OK = 200;
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';
    const METHOD_PUT = 'PUT';
    const METHOD_DELETE = 'DELETE';
    
    protected $key       = null;
    protected $secret    = null;
    protected $host      = null;
    protected $request   = null;
    protected $ssl_certificate = null;
       
    public function __construct($host, $key, $secret, $ssl_certificate = null) 
    {
        $this->host   = $host;
        $this->key    = $key;
        $this->secret = $secret;
        $this->ssl_certificate = $ssl_certificate;
        $this->request = new Curl($this->host, $this->ssl_certificate);
        $this->request->setClientCredentials($this->key, $this->secret);
    }
    
    public function authenticate($user_hash) 
    {
        $this->request->addHttpHeader('X-Auth-User-ID: '.trim($user_hash));
        return true;
    }
    
    
    /**
     * Consume resource from OP
     * 
     */
    public function consume($method, $api, array $params = []) 
    {
       $response = $this->request->factory($method, $api, $params);
       if (Connection::OK != $response->status) {
           throw new Exception($response->head['message'], array(), $response->status);
       }
       return $response->body; 
    }
}