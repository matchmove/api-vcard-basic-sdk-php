# PHP SDK for OP integration with Basic Authentication support #

- version: 1.0.0

### Installation: ###

1. Clone this repository.

2. Create a bootstrap script containing:

        require 'vcard/matchmovepay.php';
        MatchMovePay::init();

3. Define the connection details like, the target host, consumer_key, consumer_secret. You can also define the SSL certificate location when required. You can follow this [reference](http://unitstep.net/blog/2009/05/05/using-curl-in-php-to-access-https-ssltls-protected-sites)

    The host for connecting to testing is, `https://beta-api.mmvpay.com/<product>/v1` while `https://api.mmvpay.com/<product>/v1` should be used for production.
    
        $conn = new MatchMovePay\VCard\Connection('https://api.mmvpay.com/sg/v1', $key, $secret);
    or,
    
        $conn = new MatchMovePay\VCard\Connection('https://api.mmvpay.com/sg/v1', $key, $secret, $path_to_certificate);

4. From here on, you can,

    * Consume .

        $response = $conn->consume('POST', 'users', ['email' => 'some@one.com', ... ]);
